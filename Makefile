NAME          := firmware

# Utility commands
RM            := rm -rf
DIR_DUP       = mkdir -p $(@D)

# Compiler commands
GCC_PREFIX    := arm-none-eabi
CC            := $(GCC_PREFIX)-gcc
OBJCOPY       := $(GCC_PREFIX)-objcopy

# Directories
SRC_DIR       := Src
OBJ_DIR       := Obj
BUILD_DIR     := Build
INC_DIRS      := $(shell find . -name "*.h" | xargs dirname | uniq | sed 's/^\.\///')

# Output files
ELF           := $(BUILD_DIR)/$(NAME).elf
MAP           := $(BUILD_DIR)/$(NAME).map
BIN           := $(BUILD_DIR)/$(NAME).bin

# Input files
SRCS          := $(shell find . -name "*.c" -or -name "*.s" | sed 's/^\.\///')
INCS          := $(shell find . -name "*.h" | sed 's/^\.\///')
LINKER_SCRIPT := $(wildcard *.ld)

OBJS_TMP      := $(SRCS:%.c=$(OBJ_DIR)/%.o)
OBJS          := $(OBJS_TMP:%.s=$(OBJ_DIR)/%.o)

# Flags
IFLAGS        := $(addprefix -I./, $(INC_DIRS))
OPTIMISATION  := -O0
CFLAGS        := -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F302x8 $(OPTIMISATION) -ffunction-sections -fdata-sections -Wall --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb
LFLAGS        := -mcpu=cortex-m4 -T"$(LINKER_SCRIPT)" --specs=nosys.specs -Wl,-Map="$(MAP)" -Wl,--gc-sections -static --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wl,--start-group -lc -lm -Wl,--end-group

# Recipes
all: $(BIN)

$(BIN): $(ELF)
	$(OBJCOPY) -O binary $< $@

$(ELF) $(MAP): $(OBJS) $(LINKER_SCRIPT) Makefile
	$(DIR_DUP)
	$(CC) -o $(ELF) $(OBJS) $(LFLAGS)

$(OBJ_DIR)/%.o: %.c
	$(DIR_DUP)
	$(CC) "$<" $(CFLAGS) -c $(IFLAGS) -o "$@"

$(OBJ_DIR)/%.o: %.s
	$(DIR_DUP)
	$(CC) "$<" $(CFLAGS) -c $(IFLAGS) $(OPTIMISATION) -o "$@"

clean:
	$(RM) $(OBJ_DIR) $(BUILD_DIR)

.PHONY: clean
